# Starter Syfmony with docker
Une Api d'un miniblog avec api-platform

# Prérequis
* dokcer
* docker-compose
* make

# Lancer les containers docker
```bash
make start
```

# Créer le projet Symfony
```bash
make ssh-php
symfony new -full .
```

# Installer les dépendances si vous récupérer le projet depuis un dépôt
``` Bash
make vendor-install
```

# Modifier les droit de l'utilisateur local afin de pouvoir modifier les fichier du contianer
```bash
cd app/
sudo chown -R $USER ./
```

# Configurer la base de données 
Dans .env repmlacer la variable d'environnement DATABASE_URL par : DATABASE_URL=mysql://root:@db:3306/miniblog?serverVersion=5.7

# Ouvrir le projet dans un navigatuer
http://localhost:8888/

# Ouvrir le phpMyAdmin
http://localhost:9999/

# Ouvrir le serveur de mail -MailCatcher
http://localhost:1080/



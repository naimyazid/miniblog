<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\User;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{   
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($u=0; $u<10; $u++)
        {
            $user = new User();
            $hashedPassword = $this->passwordHasher->hashPassword($user, 'password');
            
            $user->setEmail($faker->email())
                 ->setPassword($hashedPassword);

            $manager->persist($user);

            for ($a=0; $a<random_int(5,15); $a++)
            {
                $article = new Article();
                $article->setName($faker->text(50))
                        ->setContent($faker->text(300))
                        ->setAuthor($user);
                $manager->persist($article);
            }
        }

        $manager->flush();
    }
}
